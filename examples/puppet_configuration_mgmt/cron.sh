#!/bin/bash

puppet resource cron puppet-agent command='/opt/puppetlabs/bin/puppet agent --no-daemonize --onetime --logdest syslog' user=root minute='*/30'
