user { 'git':
  ensure     => present,
  managehome => true,
}

file { '/home/git/.ssh':
  ensure => 'directory',
  owner  => 'git',
  group  => 'git',
  mode   => '0700',
}

file { '/home/git/.ssh/authorized_keys':
  ensure => 'file',
  owner  => 'git',
  group  => 'git',
  mode   => '0600',
  require => File['/home/git/.ssh']
}

exec { 'create-root-sshkey':
  path => '/bin',
  command => 'ssh-keygen -f /root/.ssh/id_rsa -N ""',
  creates => '/root/.ssh/id_rsa',
}
