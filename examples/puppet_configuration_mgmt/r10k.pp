#
# Central repository setup
#

# create the git user for accessing the central repository
user { 'git':
  ensure     => present,
  managehome => true,
}

# create .ssh dir and add the root ssh public key to
# authorized_keys
file { '/home/git/.ssh':
  ensure => 'directory',
  owner  => 'git',
  group  => 'git',
  mode   => '0700',
} ->
file { '/home/git/.ssh/authorized_keys':
  ensure => 'present',
  owner  => 'git',
  group  => 'git',
  mode   => '0600',
  source => 'puppet:///examples/id_rsa.pub',
}

# create .ssh for the root user and deploy the private and public
# key
file { '/root/.ssh':
  ensure => 'directory',
  owner  => 'root',
  group  => 'root',
  mode   => '0700',
} 

file { '/root/.ssh/id_rsa':
  ensure  => 'present',
  owner   => 'root',
  group   => 'root',
  mode    => '0600',
  source  => 'puppet:///examples/id_rsa',
  require => File['/root/.ssh']
} 

file { '/root/.ssh/id_rsa.pub':
  ensure  => 'present',
  owner   => 'root',
  group   => 'root',
  mode    => '0600',
  source  => 'puppet:///examples/id_rsa.pub',
  require => File['/root/.ssh']
}

# initialize the control-repo, this is a copy of 
# a template puppet.com provides
exec { 'git-init-control':
  path        => '/bin',
  command     => 'git clone --bare https://github.com/puppetlabs/control-repo.git',
  cwd         => '/home/git/',
  creates     => '/home/git/control-repo.git',
  user        => 'git',
  group       => 'git',
}

# initialize the example module we would like to 
# deploy with r10k
exec { 'git-init-module':
  path        => '/bin',
  command     => 'git clone --bare https://gitlab.com/tosmi/packt-pcm.git',
  cwd         => '/home/git/',
  creates     => '/home/git/packt-pcm.git',
  user        => 'git',
  group       => 'git',
}

# install r10k and create a configuration that
# points to our local control repository
class { 'r10k':
  remote => 'git@localhost:control-repo.git',
  provider => 'puppet_gem',
}
