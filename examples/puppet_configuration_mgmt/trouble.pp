exec { 'git-init-module':
  path        => '/bin',
  command     => 'git clone --bare https://gitlab.com/tosmi/packt-trouble.git',
  cwd         => '/home/git/',
  creates     => '/home/git/packt-trouble.git',
  user        => 'git',
  group       => 'git',
}
