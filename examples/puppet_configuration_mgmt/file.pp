file { '/tmp/testfile.txt':
  ensure => present,
  source => 'puppet:///example/testfile.txt',
}
