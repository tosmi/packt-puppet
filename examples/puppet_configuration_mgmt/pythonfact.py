#!/usr/bin/env python
#
import json

data = { "pythonkey1" : { 
           "pythonsubkey1": "subvalue1", 
           "pythonsubkey2": "subvalue2", 
           },
         "pythonkey2" : "value2" 
       }

print json.dumps(data)
