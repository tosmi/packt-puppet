$message = $facts['os']['family'] ? {
  'RedHat'   => 'running on RedHat',
  'YellowHat' => 'running on YellowHat',
  default   => 'running somewhere',
}

notify { $message: }
