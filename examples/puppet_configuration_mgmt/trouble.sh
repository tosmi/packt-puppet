#!/bin/bash

set -eof pipefail

export PATH=/opt/puppetlabs/bin:/bin

puppet apply --fileserverconfig /vagrant/examples/puppet_configuration_mgmt/fileserver_ssh.conf /vagrant/examples/puppet_configuration_mgmt/trouble.pp  
