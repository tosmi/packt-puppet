user { 'graph': 
  ensure => 'present',
}

file { '/tmp/graph':
  ensure  => 'present',
  owner   => 'graph',
  group   => 'graph',
  require => User['graph']
}

package { 'puppetserver':
  ensure => 'present',
}
