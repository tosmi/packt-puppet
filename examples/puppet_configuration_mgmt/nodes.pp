$variable = 'top'

node 'puppet-vm1.lan.stderr.at' {
  $variable = 'node'
  notify { "hello vm1 scope, variable has the value ${variable}": }
}

node /vm\d+/ {
  notify { "hello vm2 scope, variable has the value ${variable}": }
}

node default {
  notify { 'hello default scope': }
}
