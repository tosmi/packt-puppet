#!/opt/puppetlabs/puppet/bin/ruby
#
# a very simple puppet external node classifier, just for
# demonstration
#
# (c) toni@stderr.at 2017

require 'yaml'

node = ARGV.empty? ? 'unknown' : ARGV.shift

classes = {
  'classes' => {
    'simplemodule' => {
      'parameter' => node
    }
  }
}

puts classes.to_yaml
